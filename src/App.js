import React, {Component} from "react";
import {
  BrowserRouter as Router,
  Route,
  Routes,
} from "react-router-dom";
import HomePage from "./HomePage";
import './App.css';

class App extends Component{
  render(){
    return(
      // <Router>
      //   <Routes>
      //     <Route exact path="/" component={HomePage} />
      //     <Route exact path="/education" component={EducationPage} />
      //     <Route exact path="/skills" component={SkillsPage} />
      //     <Route exact path="/portfolio" component={PortfolioPage} />
      //   </Routes>
      // </Router>
      <div>
        <HomePage />
      </div>
    )
  }
}

export default App;
