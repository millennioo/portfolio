import React, {Component} from 'react';
import { Col, Row, Space} from 'antd';

import '../App.css';

// const { Link } = Typography;
// const { TabPane } = Tabs;

class Header_home extends Component{

  render(){
    return(
      <div>
        <Row className="header_bar">
            <Col xs={7} sm={9} md={9} lg={9} xl={9} style={{textAlign:"right"}}>
              <Space align="baseline">
                <span>
                  <a class="header_text" href="/">HOME</a>
                </span>
              </Space>
            </Col>
            <Col xs={6} sm={4} md={4} lg={4} xl={4} style={{textAlign:"center"}}>
              <Space align="baseline">
                <span>
                  <a class="header_text" href="/education">EDUCATION</a>
                </span>
              </Space>
            </Col>
            <Col xs={4} sm={2} md={2} lg={2} xl={2} style={{textAlign:"left"}}>
              <Space align="baseline">
                <span>
                  <a class="header_text" href="/skills">SKILLS</a>
                </span>
              </Space>
            </Col>
            <Col xs={7} sm={9} md={9} lg={9} xl={9} style={{textAlign:"left" }}>
              <Space align="baseline">
                <span>
                  <a class="header_text" href="/portfolio">PORTFOLIO</a>
                </span>
              </Space>
            </Col>
        </Row>
      </div>
    )
  }
}

export default Header_home;

